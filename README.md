# What is this?

This is a write-up of privacy concerning applications and their superior **privacy-respecting** counterparts and services. All suggestions here do not require root access unless otherwise specified.

Contrary to popular belief you can outfit your phone with completely functional and, in most ways *superior*, FOSS applications.

#Glossary of commonly used words some of which may be used in this article.
- E2EE
> End-to-end encryption
- FOSS
> Free open source
- GPL vX
> GNU Public License vX (1-3)
- OS
> Operating System
- Warrant Canary
> A warrant canary is a method by which a communications or other secure service provider aims to inform its users that the provider has been served with a secret government subpoena despite legal prohibitions on revealing the existence of the subpoena.

Apps/services must meet these requirements to be a viable replacement candidate:

- Open-source because we cannot verify what closed source apps are really doing
- Does not collect user data or absolutely minimal that can't identify user
- Easy to setup

I, **Nikolaus Gullotta**, am not affiliated with any apps or services listed below.

# Table of contents

* [OS](https://bitbucket.org/zombocom/mobile-privacy-guide/src/master/#markdown-header-operating-system-os)

* [App Stores](https://bitbucket.org/zombocom/mobile-privacy-guide/src/master/#markdown-header-app-stores)

* [Browsers](https://bitbucket.org/zombocom/mobile-privacy-guide/src/master/#markdown-header-browsers)

* [Search Engines](https://bitbucket.org/zombocom/mobile-privacy-guide/src/master/#markdown-header-search-engines)

* [Messengers](https://bitbucket.org/zombocom/mobile-privacy-guide/src/master/#markdown-header-messengers)

* [Email Providers](https://bitbucket.org/zombocom/mobile-privacy-guide/src/master/#markdown-header-email-providers)

* [VPN Providers](https://bitbucket.org/zombocom/mobile-privacy-guide/src/master/#markdown-header-vpn-providers)

* [Cloud Services](https://bitbucket.org/zombocom/mobile-privacy-guide/src/master/#markdown-header-cloud-services)

* [File Sharing Apps](https://bitbucket.org/zombocom/mobile-privacy-guide/src/master/#markdown-header-file-sharing-apps)

* [Password Managers](https://bitbucket.org/zombocom/mobile-privacy-guide/src/master/#markdown-header-password-managers)

* [Note-taking Apps](https://bitbucket.org/zombocom/mobile-privacy-guide/src/master/#markdown-header-note-taking-apps)

* [YouTube Alternatives](https://bitbucket.org/zombocom/mobile-privacy-guide/src/master/#markdown-header-youtube-alternatives)

* [Ad blocking on Android](https://bitbucket.org/zombocom/mobile-privacy-guide/src/master/#markdown-header-ad-blocking-on-android)

* [Miscellaneous Apps](https://bitbucket.org/zombocom/mobile-privacy-guide/src/master/#markdown-header-miscellaneous-apps)


#Operating System (OS)
You should not use the stock ROM that comes on your device. Stock ROMs includes **proprietary** (like Google Apps such as the Play Store and [Google Play Services](https://en.wikipedia.org/wiki/Google_Play_Services)) apps and services that **spy on you**.  Google even tracks your location when it's [turned off](https://www.techdirt.com/articles/20171121/09030238658/investigation-finds-google-collected-location-data-even-with-location-services-turned-off.shtml).

Operating Systems listed below will not spy you.

[LineageOS](https://www.lineageos.org/)
> Operating system that respects your freedom.

[RattleSnakeOS](https://github.com/dan-v/rattlesnakeos-stack)
> Privacy focused Android OS with advanced security features.

> NOTE: You need to build this by yourself.

[microG](https://microg.org)
> Open-source alternative for Google Play Services.

> You can also check out [LineageOS for microG](https://lineage.microg.org).

> Note that **microG isn't operating system**.

[Magisk](https://forum.xda-developers.com/apps/magisk)
> Open-source and most popular root method.

> WARNING: **DO NOT** use SuperSU. **It's closed source and owned by Chinese company**

> Note that **Magisk isn't operating system**.

[AsteroidOS](https://asteroidos.org/)
> Open-source operating system for smartwatches.

# App Stores
Do not use Google Play Store. It collects information about your installed apps and Google even has ability to [**remove and install apps without your permissions**](https://jon.oberheide.org/blog/2010/06/25/remote-kill-and-install-on-google-android/). If you absolutely *must* use Google Play because you're stupid, neuter it's permissions to only storage.

App stores that are listed below are open-source and respect your freedom.

[F-Droid](https://f-droid.org/en/)
> Community-maintained software repository of FOSS (*Free and Open-Source Software*) apps.

> You can also add your own respositories.

[YalpStore](https://github.com/yeriomin/YalpStore)
> YalpStore allows you to download apps from Google Play Store without violating your privacy.

> You can also check [AuroraStore](https://gitlab.com/AuroraOSS/AuroraStore) which is fork of YalpStore with material design.


#Browsers
[Do not use Google Chrome](https://spyware.neocities.org/articles/chrome.html). It's huge spyware and **will track everything you do online**. 

Browsers that are listed below will not track you and are open-source.

[Brave](https://www.brave.com/)
> Chromium based browser with built-in adblocking, noscript, and anti-fingerprinting tools

[Bromite](https://www.bromite.org/)
> Chromium based browser with built-in ad blocking and privacy enhancements.

[Fennec F-Droid](https://f-droid.org/en/packages/org.mozilla.fennec_fdroid/)
> Firefox-based browser which removes the proprietary bits found in official Firefox.

[Firefox Klar](https://f-droid.org/en/packages/org.mozilla.klar/)
> Surf and forget. Klar will delete all your data when you exit browser.

> WARNING: Firefox Klar version have WebRTC leak and it cannot be disabled.

[Privacy Browser](https://www.stoutner.com/privacy-browser/)
> Privacy Browser protects your privacy by disabling features like JavaScript, DOM storage, and cookies that are used by websites to track users.

> WARNING: Privacy Browser is susceptible to [MITM attacks when browsing insecure websites](https://www.stoutner.com/kitkat-security-problems/) from devices running Android KitKat.


# Search Engines

[Do not use Google search](https://spyware.neocities.org/articles/google.html). **It builds a profile from your searches** and knows your location. 

Search Engines listed below do not build a profile about you.

[StartPage](https://www.startpage.com/)

> Uses Google search to provide results. Google will only see StartPage, it will not see you.

[DuckDuckGo](https://duckduckgo.com/)
> DuckDuckGo doesn't save your searches or your location. Provides Yahoo and Bing results.

> WARNING: Based in the [US](https://www.privacytools.io/#ukusa) and hosted on Amazon servers.

[Searx](https://searx.me/)
> Provides search results from multiple search engines, including Google search. Run by an individual.

> If you don't trust individual persons then use [Searx by Disroot](https://search.disroot.org/).

[Qwant](https://www.qwant.com/)
>Europe Based flashier alternative than most search engines.


# Messengers
Do not use closed source messengers like WhatsApp, Hangouts or Threema. We can't verify what they are really doing in background.

Messengers that are listed below are open-source and are encrypted so no one can read your messages.

[Signal](https://signal.org/)
> Encrypted messenger & calling app. Doesn't collect information about users.

> WARNING: Signal is based in the [US](https://www.privacytools.io/#ukusa).

[Telegram](https://telegram.org/)
> Encrypts messages, doesn't collect info on users. Alternative to GroupMe

[Wire](https://wire.com/en/)

> Switzerland based company and doesn't collect information about users. Can also make calls.

[Conversations] (https://conversations.im) - [Free on F-Droid] (https://f-droid.org/en/packages/eu.siacs.conversations/)
> Client for Jabber/XMPP protocols. Can't make calls.

> You can host your own XMPP server or select from trusted providers. Good lists of providers are the [Official list from Conversations](https://conversations.im/compliance/) or the [Public XMPP servers](https://list.jabber.at) list.

Some trusted providers:
> [XMPP.is] (https://xmpp.is/)

> Based in Germany and doesn't collect users messages or IP addresses.

[Matrix](https://matrix.org/) - [Download client on F-Droid](https://f-droid.org/en/packages/im.vector.alpha/)
> Matrix is open-source decentralised protocol, it can do 1on1 and group chat with support for end to end encryption. Supports voice and video calls. There are gateways to chat across different networks.

> There are [multiple clients](https://matrix.org/docs/projects/try-matrix-now.html). The official one is called Riot, which is provided as [desktop app](https://riot.im/desktop.html), mobile app and [web client](https://riot.im/app/).

> WARNING: Official Matrix server collects a lot of metadata. Consider using another provider or host your own.

[Briar](https://briarproject.org/index.html)
> Doesn't rely on a central server and works without Internet (through Bluetooth or Wi-FI). Also hides metadata. Can't make calls. 

> WARNING: I'm not sure how reliable this messenger is and how many bugs it's have.

[Silence](https://silence.im/)
> Silence is a full replacement for the default text messaging app. Encrypts your communications between other Silence users.

> WARNING: For non-Silence users communications isn't encrypted.

[QKSMS](https://github.com/moezbhatti/qksms)
> QKSMS is an replacement to the stock messaging app

> WARNING: Communications isn't encrypted.

# Email Providers

Do not use Gmail. It's owned by Google which **scans all of your emails**. 

Email providers that are listed below do not read your emails.

[ProtonMail](https://protonmail.com/)
> Encrypted email provider based on Switzerland.

> WARNING: Doesn't encrypt metadata.

[Tutanota](https://tutanota.com/)
> Open-source encrypted email provider located in Germany. Also encrypts metadata. Free plan with 1GB storage. 

> Also have [paid plans](https://tutanota.com/pricing).

> Tutanota offers [beta app](https://tutanota.uservoice.com/knowledgebase/articles/483300-where-can-i-get-the-tutanota-app) that works without Google Play Services. [They are planning to publich app on F-Droid too](https://tutanota.com/blog/posts/secure-mail-open-source).

[Posteo](https://posteo.de/en)
> Email provider that doesn't collect your personal information. Based on Germany. *1€/month*

> WARNING: Encryption isn't on by default.

[K-9 Mail](https://k9mail.github.io)
> K-9 Mail is an email client.

[OpenKeychain](https://www.openkeychain.org/)
> OpenKeychain uses encryption to ensure that your messages can be read only by the people you send them to.

> Primarily integrates with K-9 Mail to provide end-to-end encryption capabilities.

# VPN Providers
Getting good VPN is important on Android.

All VPN providers listed below have **"no logging" policy**.

> NOTE: You can never fully trust a VPN service. **There have been many cases where a VPN service claimed not to collect logs but still logged everything**. 

> You are just **moving your trust from your ISP to the VPN provider**. If you need real anonymity use [Tor](https://www.torproject.org) or [Tails](https://tails.boum.org).

[ProtonVPN](https://protonvpn.com/)
> Based in Switzerland. [3 paid plans](https://protonvpn.com/pricing).

> There is also free plan but it's limited.

[Mullvad](https://mullvad.net/en/)
> Based in Sweden. €5/month and supports Bitcoin.

[SigaVPN](https://sigavpn.org/)
> Based in the [US](https://www.privacytools.io/#ukusa). Doesn't cost anything, but you can get extra services for donating.

> WARNING: Service is based in the [US](https://www.privacytools.io/#ukusa).

> It's free, but the developer has said that it [doesn't log anything](https://sigavpn.org/privacy.html) and is run by donations and users that cryptomine for the service (optional).

>*Use with caution*!

[OpenVPN](https://openvpn.net/) - [Download on F-Droid](https://f-droid.org/en/packages/de.blinkt.openvpn/)
> OpenVPN is client for connecting to your VPN Service through configuration files.

**VPN Services to avoid**:

These services are known to be harmful to your privacy and should never be used.

- IPVanish

- Hotspot Shield

- PureVPN

- Private Internet Access\*

\*PIA **is not known to be harmful** for your privacy, but **it does have some bad points to consider**:
- PIA recently [hired *Mark Karpeles* as CTO](https://www.reddit.com/r/PrivateInternetAccess/comments/8eejmr/what_the_fuck_is_pia_thinking_hiring_mark/).
- PIA is based in the [US](https://www.privacytools.io/#ukusa). Yes, *SigaVPN is based in the [US](https://www.privacytools.io/#ukusa) too*, but **when you pay for the VPN service you need to get the best privacy possible** and with PIA **you won't get it**.


# Cloud services 

Do not use Google Drive for cloud storage solutions. It's owned by Google and **reads all your files**. 
Google Photos is not recommeded either because Google scans all your photos and videos.

All cloud services listed below do not access your files.
**Always encrypt your files before uploading them to cloud services**.

[Syncthing](https://syncthing.net/)
> Synchronizes your data between multiple devices. There is no central server that might get compromised as it is self hosted. Supports async modification and file versioning.

[Nextcloud](https://nextcloud.com/)
> You can host your own Nextcloud or use one of the trusted providers below.

[Cryptee](https://crypt.ee/)
> Cryptee is a cross-platform, zero-knowledge, client-side AES256 encrypted, Documents and Photos service.

> You can sign up with just a username.

Nextcloud Providers:

> [Woelkli] (https://woelkli.com/en) FREE/PRO.

> [Disroot] (https://disroot.org/en/services/nextcloud) 4GB/FREE.


# File Sharing Apps
Transfer your files securely between devices.

[TrebleShot](https://github.com/genonbeta/TrebleShot)
> TrebleShot allows you to send and receive files without an internet connection.

[NitroShare](https://nitroshare.net/) - [Download on F-Droid](https://f-droid.org/en/packages/net.nitroshare.android/)
> NitroShare is completely free of ads and trackers. Works on multiple platforms.

[KDE Connect](https://community.kde.org/KDEConnect)
> Integrates your Android phone with KDE desktop environment.

# Password Managers
Creating strong passwords is an important part of privacy & security so that your accounts are more difficult to compromise.

[Edward Snowden on passwords](https://www.invidio.us/watch?v=yzGzB-yYKcc).

All password managers listed below are open-source.

[KeePassXC](https://github.com/keepassxreboot/keepassxc)
> Fork of popular KeePass for Android. Online browser extension (not reccomended). More feature-ful and actively maintained.

[KeePass DX](https://github.com/Kunzisoft/KeePassDX)
> Fork of popular KeePass for Android. Offline only.

[Bitwarden](https://bitwarden.com/)
> Bitwarden can sync your passwords across all of your devices.

[Passit](https://passit.io/)
> Passit is an open-source, cloud-based password manager.

[Password Store](https://f-droid.org/en/packages/com.zeapo.pwdstore/)
> Can be synced with your cloud provider.

> Uses GPG key to encrypt data.


# Note-taking apps
You shouldn't use Google Keep or evernote because Google reads all your note and Evernote scrapes data for targeted ads.

Note-taking apps that are listed below do not read your notes.

[Joplin](https://joplin.cozic.net/)
> Open-source and encrypted note-taking and to-do application. Can sync between devices. 

> Good replacement for Evernote.

[Standard notes](https://standardnotes.org/)
> Encrypted note-taking app that can sync your notes across all of your devices.

[Simple Notes](https://github.com/SimpleMobileTools/Simple-Notes)
> Local note-taking app. Doesn't have encryption.


# YouTube Alternatives
Watch YouTube videos without harming your privacy.

App: [NewPipe](https://newpipe.schabi.org)

> Watch YouTube on your smartphone without annoying ads and questionable permissions.

App: [SkyTube](http://skytube-app.com/)

> SkyTube is an alternative, free, open-source YouTube application for Android.

Website: [Invidio](https://www.invidio.us)

> Invidio is an alternative to HookTube.

Website: [CloudTube](https://cadence.gq/cloudtube/search)

> When watching a video, no contact is made with the YouTube API.

# Ad Blocking on Android

[Blokada](https://blokada.org)
> Open-source ad blocker. Use [AdAway](https://adaway.org) if you have *rooted device*. 

> User can also change DNS address on Blokada.

[NetGuard](https://www.netguard.me)
> Block apps from accessing Internet. Use [AFWall+](https://github.com/ukanth/afwall) if you have *rooted device*. 

> User can also change DNS addresses on both apps.

[XPrivacyLua](https://f-droid.org/en/packages/eu.faircode.xlua/)
> Feeds apps with fake data instead of your real personal data.

> *XPrivacyLua is not ad blocking app but it will reduse risk of your personal data to leak*.

> **Root and Xposed required**.

Privacy respecting DNS servers:

>[UncensoredDNS](https://blog.uncensoreddns.org)

> Based in Denmark and doesn't log users (*only logs traffic volume*).

>[SecureDNS](https://securedns.eu)

> Based in Netherlands and doesn't log users.


**DO NOT** use [Cloudfare's DNS](https://www.reddit.com/r/sevengali/comments/8fy15e/) servers. Cloudfare is not [privacy respecting](https://www.reddit.com/r/privacy/comments/41cb4k/be_careful_with_cloudflare/) company. 


# Miscellaneous Apps

Gboard
> [AnySoftKeyboard](https://anysoftkeyboard.github.io) doesn't collect information about users.

> [Simple Keyborad](https://f-droid.org/en/packages/rkr.simplekeyboard.inputmethod/) for users that don't need extra features.

> [Hacker's Keyboard](https://f-droid.org/en/packages/org.pocketworkstation.pckeyboard/) with advanced features.

> AOSP keyboard that comes pre-installed with many Custom ROMs.

[Simple Mobile Tools](https://simplemobiletools.github.io/)
> A group of simple and open-source Android apps without annoying ads and unnecessary permissions.

[Open Camera](https://opencamera.sourceforge.io/)
> Completely free, and no ads.

[OsmAnd](https://osmand.net)
> Maps & Navigation app that respects your privacy.

[andOTP](https://github.com/andOTP/andOTP)
> Use this app to control your two-factor authentication codes.

> Fork of OTP Authenticator.

[EDS Lite](https://f-droid.org/en/packages/com.sovworks.edslite/)
> EDS allows you to store your files in an encrypted container.

[LibreTorrent](https://gitlab.com/proninyaroslav/libretorrent)
> Torrent client for Android with advanced features.

[mpv](https://github.com/mpv-android/mpv-android)
> Video player based on libmpv.

[Slide for Reddit](https://github.com/ccrama/Slide)
> Open-source, ad free Reddit browser.

[Pedometer](https://github.com/j4velin/Pedometer)
> Pedometer app to count your steps.

[Voice](https://f-droid.org/en/packages/de.ph1b.audiobook/)
> Audiobook player.

[AntennaPod](http://antennapod.org/)
> Podcast manager and player that gives you instant access to millions of free and paid podcasts.

#Social Media
I do not use social media much. Use them all at your own peril.


To start, I can't believe I have to say this but...
## **DO NOT USE FACEBOOK**
Facebook is known for **listening to your microphone without your permission** (there isn't any **official** proof, only many reports) and gathering all of your data. They have clearly demonstrated a lack of care for all users data and even helped destabalize the US election.

> Two well explained Reddit posts about Facebook listening: [Post 1](https://www.reddit.com/r/privacy/comments/7mxn9i/is_facebook_listening_and_creating/) and [Post 2](https://www.reddit.com/r/privacy/comments/7n4b8g/part_2_of_facebook_listening_test_listening_to_a/).

**IF YOU ABSOLUTELY MUST USE FACEBOOK USE THE FROST CLIENT, AVAILABLE ON F-DROID. DO NOT EVER PUT ANYTHING ON FACEBOOK BESIDES THE MOST BASIC INFO.**

[Twidere](https://f-droid.org/en/packages/org.mariotaku.twidere/)
> Alternative to stock Twitter

There are no other FOSS app specific alternatives for using things like: Snapchat, Instagram, Discord. Use them at your own peril, and neuter their permissions.